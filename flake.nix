{
  description = "My website :)";

  outputs = { self, nixpkgs }: let system = "x86_64-linux"; pkgs = nixpkgs.legacyPackages.x86_64-linux; in {
    devShells.${system}.default = pkgs.mkShell {
      nativeBuildInputs = with pkgs; [
        lighttpd
        vscode-langservers-extracted
      ];
    };
  };
}
